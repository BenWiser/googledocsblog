# GoogleDocsBlog

This is my blog generator for https://benwiser.com/blog/

## Environment
- This is a kotlin project and uses gradle so first make sure you have the appropriate environment set up for that (https://kotlinlang.org/)

## Setup
- Setup, you need to create a service account
- Give that service account full access to your blog folder on Google Drive
- Then store the `credentials.json` generated in `/src/main/resources`
- Run `cp /src/main/resources/example-settings.properties /src/main/resources/settings.properties` and provide your folder id
- `./gradlew run --args="Path/To/Blog"`
   - It defaults to point to a `style.css`, be sure to include that for any customized styling
- To update how the page structure looks update the `template.html`