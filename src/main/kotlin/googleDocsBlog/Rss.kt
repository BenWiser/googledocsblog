package googleDocsBlog

import org.thymeleaf.TemplateEngine
import org.thymeleaf.context.Context
import org.thymeleaf.templatemode.TemplateMode
import org.thymeleaf.templateresolver.StringTemplateResolver
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

data class RssFeed(val blogs: List<com.google.api.services.drive.model.File>)

fun renderRss(rssFeed: RssFeed): String {
    val template = File("rss.template.xml").readText(Charsets.UTF_8)
    val context = Context()

    val sdf = SimpleDateFormat("dd/M/yyyy hh:mm:ss")
    val currentDate = sdf.format(Date())
    context.setVariable("lastBuildDate", currentDate)

    context.setVariable("blogs", rssFeed.blogs)

    val templateEngine = TemplateEngine()
    val templateResolver = StringTemplateResolver()
    templateResolver.templateMode = TemplateMode.XML
    templateEngine.setTemplateResolver(templateResolver)
    return templateEngine.process(template, context)
}